from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        'title' : 'Favourites',
    }
    return render(request, 'social/index.html', context)