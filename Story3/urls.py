from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('AboutMe/', include('AboutMe.urls')),
    path('Experiences/', include('Experiences.urls')),
    path('Favourites/', include('social.urls')),
]
