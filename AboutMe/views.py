from django.shortcuts import render

# Create your views here.
def index(request):
    biodata = ['Name: Hasna Nadifah', 'Status: Student', 'NPM: 1906293096', 'Year: 2019', 'Hobby: Watching Movies', 'Email: hnadifah@gamil.com' ]
    context = {
        'title' : 'About Me',
        'biodata' : biodata
    }
    return render(request, 'AboutMe/index.html', context)