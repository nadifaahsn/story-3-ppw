from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        'title' : 'Experiences',
        'pageTitle' : 'Ini experiences'
    }
    return render(request, 'Experiences/index.html', context)